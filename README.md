# Nama testing skills

This is a project to test my developing skill. It should execute a simple function.
Tests and coding styles should be implemented.

## Usage

To run a server and check the results use 

``
npm start
``

Then access http://localhost:8080.

For testing run 

``
npm test
`` 

For watch testing run

``
npm run test:watch
``