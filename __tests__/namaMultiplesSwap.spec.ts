import { namaMultiplesSwap } from '../utils/namaMultiplesSwap';

describe('Testing namaMultiplesSwap', (): void => {
  describe('Edge cases', (): void => {
    it('should return empty string for 0', (): void => {
      const value = 0;

      const expectedResult = '';

      const result = namaMultiplesSwap(value);

      expect(result).toBe(expectedResult);
    });
    it('should return error for floating point number', (): void => {
      const value = 10.5;

      try {
        namaMultiplesSwap(value);
        expect(true).toBeFalsy();
      } catch (err) {
        expect(err.message).toBe('Value cannot be a floating point number');
      }
    });

    it('should return error for negative number', (): void => {
      const value = -10;

      try {
        namaMultiplesSwap(value);
        expect(true).toBeFalsy();
      } catch (err) {
        expect(err.message).toBe('Value cannot be a negative number');
      }
    });
  });
  describe('Use cases', (): void => {
    it('should match results for number 100', (): void => {
      const value = 100;

      const expectedResult =
        '1, 2, 3, 4, Nama, 6, Team, 8, 9, Nama, 11, 12, 13, Team, Nama, 16, 17, 18, 19, Nama, Team, 22, 23, 24, Nama, 26, 27, Team, 29, Nama, 31, 32, 33, 34, Nama Team, 36, 37, 38, 39, Nama, 41, Team, 43, 44, Nama, 46, 47, 48, Team, Nama, 51, 52, 53, 54, Nama, Team, 57, 58, 59, Nama, 61, 62, Team, 64, Nama, 66, 67, 68, 69, Nama Team, 71, 72, 73, 74, Nama, 76, Team, 78, 79, Nama, 81, 82, 83, Team, Nama, 86, 87, 88, 89, Nama, Team, 92, 93, 94, Nama, 96, 97, Team, 99, Nama';

      const result = namaMultiplesSwap(value);

      expect(result).toBe(expectedResult);
    });
  });
});
