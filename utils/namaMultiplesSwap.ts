export const namaMultiplesSwap = (value: number): string => {
  if (value < 0) {
    throw Error('Value cannot be a negative number');
  }

  if (value % 1 !== 0) {
    throw Error('Value cannot be a floating point number');
  }

  let values: string[] = [];
  for (let i = 1; i <= value; i++) {
    if (i % 35 === 0) {
      values.push('Nama Team');
    } else if (i % 5 === 0) {
      values.push('Nama');
    } else if (i % 7 === 0) {
      values.push('Team');
    } else {
      values.push(i.toString());
    }
  }

  return values.join(', ');
};
